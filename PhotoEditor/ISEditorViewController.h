//
//  ISEditorViewController.h
//  PhotoEditor
//
//  Created by igor Shvetsov on 26/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISResultsViewController.h"
#import "ISImageHandler.h"

@interface ISEditorViewController : UIViewController <UIImagePickerControllerDelegate>


- (void)chooseImage:(id)sender;

@property (nonatomic)         UIButton *preview;

@property (nonatomic)         UITableView *table;
@property (nonatomic, strong) ISResultsViewController *tableVC;

@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) UIImage *image;

- (void)reloadTableData;
- (void)setImageForEditing:(UIImage *)image;
- (UITableViewCell *)cellForRowAtIndex:(NSIndexPath *)index;
- (void)reloadRowsAtIndexPath:(NSIndexPath *)indexPath;

@end

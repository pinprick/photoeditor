//
//  AppDelegate.h
//  PhotoEditor
//
//  Created by igor Shvetsov on 26/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  ISPreview.m
//  PhotoEditor
//
//  Created by igor Shvetsov on 28/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import "ISPreviewButton.h"
#import "Constants.h"

@implementation ISPreviewButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel.font = [UIFont systemFontOfSize:kImagePickerFontSize];
        self.layer.borderColor = [UIColor blueColor].CGColor;
        self.layer.borderWidth = kImagePickerBorderWidth;
    }
    return self;
}

@end

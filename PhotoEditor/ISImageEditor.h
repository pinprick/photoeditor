//
//  ISImageEditor.h
//  PhotoEditor
//
//  Created by igor Shvetsov on 26/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ISImageEditor : NSObject

- (UIImage *)rotateImage:(UIImage *)image byDegree:(CGFloat)degrees;
- (UIImage *)convertImageToGrayScale:(UIImage *)image;
- (UIImage *)mirrorImageHorizontally:(UIImage *)image;

@end

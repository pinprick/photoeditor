//
//  ISFilterButton.m
//  PhotoEditor
//
//  Created by igor Shvetsov on 27/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import "ISFilterButton.h"
#import "Constants.h"

@implementation ISFilterButton


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel.font = [UIFont systemFontOfSize:kFilterButtonFontSize];
    }
    return self;
}

@end

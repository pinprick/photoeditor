//
//  Constants.h
//  PhotoEditor
//
//  Created by igor Shvetsov on 27/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Constants : NSObject

// Filter buttons constants
extern float const kFilterButtonWidth;
extern float const kFilterButtonHeight;
extern float const kFilterButtonXOffset;
extern float const kFilterButtonYOffset;
extern float const kFilterButtonRightOffset;

extern float const kFilterButtonFontSize;

// Image picker constants
extern float const kImagePickerWidth;
extern float const kImagePickerHeight;
extern float const kImagePickerXOffset;
extern float const kImagePickerYOffset;
extern float const kImagePickerBorderWidth;

extern float const kImagePickerFontSize;

// Result table constants
extern float const kResultTableCellHeight;


@end



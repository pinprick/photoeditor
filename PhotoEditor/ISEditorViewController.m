//
//  ISEditorViewController.m
//  PhotoEditor
//
//  Created by igor Shvetsov on 26/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import "ISEditorViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <CoreGraphics/CGAffineTransform.h>
#import "ISFilterButton.h"
#import "ISPreviewButton.h"
#import "Constants.h"
#import "ISImageEditor.h"
#import "ISImageHandler.h"
#import "ISImageEntity.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

@interface ISEditorViewController ()

@end

@implementation ISEditorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    Creating and layout filter buttons
    ISFilterButton *rotateButton = [[ISFilterButton alloc]
                              initWithFrame:CGRectMake(SCREEN_WIDTH - kFilterButtonWidth - kFilterButtonRightOffset, kFilterButtonYOffset, kFilterButtonWidth, kFilterButtonHeight)];
    [rotateButton setTitle:@"Rotate" forState:UIControlStateNormal];
    
    [rotateButton addTarget:self action:@selector(applyFilter:) forControlEvents:UIControlEventTouchUpInside];
    rotateButton.tag = 0;
    [self.view addSubview:rotateButton];
    
    ISFilterButton *invertColorsButton = [[ISFilterButton alloc]
                              initWithFrame:CGRectMake(rotateButton.frame.origin.x, rotateButton.frame.origin.y + kFilterButtonHeight + 10, kFilterButtonWidth, kFilterButtonHeight)];
    [invertColorsButton setTitle:@"Invert colors" forState:UIControlStateNormal];
    [invertColorsButton addTarget:self action:@selector(applyFilter:) forControlEvents:UIControlEventTouchUpInside];
    invertColorsButton.tag = 1;
    [self.view addSubview:invertColorsButton];
    
    ISFilterButton *mirrorButton = [[ISFilterButton alloc]
                                    initWithFrame:CGRectMake(invertColorsButton.frame.origin.x, invertColorsButton.frame.origin.y + kFilterButtonHeight + 10, kFilterButtonWidth ,kFilterButtonHeight)];
    [mirrorButton setTitle:@"Mirror image" forState:UIControlStateNormal];
    [mirrorButton addTarget:self action:@selector(applyFilter:) forControlEvents:UIControlEventTouchUpInside];
    mirrorButton.tag = 2;
    [self.view addSubview:mirrorButton];
    
//    Creating image picker button
    if (self.preview == nil) {
        self.preview = [[ISPreviewButton alloc] initWithFrame:CGRectMake(kImagePickerXOffset, kImagePickerYOffset, kImagePickerWidth, kImagePickerHeight)];
        [self.preview setBackgroundColor:[UIColor blueColor]];
        [self.preview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.preview setTitle:@"Choose image" forState:UIControlStateNormal];
        [self.preview addTarget:self action:@selector(chooseImage:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.preview];
    }
    
//    initializing image picker
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = NO;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    
    
//    Creating tableview and its controller
    if (self.tableVC == nil) {
        self.tableVC = [[ISResultsViewController alloc] init];
    }
    if (self.table == nil) {
        self.table = [[UITableView alloc] init];
    }
    self.table.frame = CGRectMake(0, SCREEN_HEIGHT/2.0, SCREEN_WIDTH, SCREEN_HEIGHT/2.0);
    [self.view addSubview:self.table];
    self.tableVC.delegate = self;
    self.table.delegate = self.tableVC;
    self.table.dataSource = self.tableVC;
}

#pragma mark - Image picker delegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        [self setImageForEditing:image];
        if (self.imagePicker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum(self.image, nil, nil, nil);
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Service methods

- (void)applyFilter:(id)sender {
    if (self.image == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You haven't chosen an image" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
//    ISImageEditor *imageEditor = [[ISImageEditor alloc] init];
//    UIImage *newImage = nil;
    
    ISImageEntity *entity = [[ISImageEntity alloc] init];
    entity.image = nil;
    entity.progress = 0;
    entity.shouldBeDisplayed = YES;
    entity.index = [self.tableVC.images count];
    
    [self.tableVC.images addObject:entity];
    [self.table reloadData];
    ISImageHandler *handler = [[ISImageHandler alloc] initWithDelegate:self.tableVC];
    [handler makeProcessingForImage:self.image withIndex:entity.index andFilter:((UIButton *)sender).tag];
    
   
    
    /*
    switch (((UIButton *)sender).tag) {
        case 0: {
            newImage = [imageEditor rotateImage:self.image byDegree:90];
            break;
        }
        case 1: {
            newImage = [imageEditor convertImageToGrayScale:self.image];
            break;
        }
        case 2: {
            newImage = [imageEditor mirrorImageHorizontally:self.image];
            break;
        }
        default:
            break;
    }
    [self.tableVC.images addObject:newImage];
    [self.table reloadData];
     */
}

- (void)chooseImage:(id)sender {
    [self presentViewController:self.imagePicker animated:NO completion:nil];
}

- (void)setImageForEditing:(UIImage *)image {
    self.image = image;
    [[self.preview imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [self.preview setImage:self.image forState:UIControlStateNormal];
}

- (void)reloadTableData {
    [self.table reloadData];
}

- (UITableViewCell *)cellForRowAtIndex:(NSIndexPath *)index {
    return [self.table cellForRowAtIndexPath:index];
}

- (void)reloadRowsAtIndexPath:(NSIndexPath *)indexPath {
    [self.table reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}



@end

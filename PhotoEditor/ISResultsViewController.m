//
//  ISResultsViewController.m
//  PhotoEditor
//
//  Created by igor Shvetsov on 26/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import "ISResultsViewController.h"
#import "ISImageCell.h"
#import "ISEditorViewController.h"
#import "Constants.h"
#import "ISImageEntity.h"

@interface ISResultsViewController ()

@end

@implementation ISResultsViewController


- (id)init {
    self = [super init];
    if (self) {
        if (_images == nil) {
            _images = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.images count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"reuse_identifier";
   
    ISImageCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (cell == nil) {
        cell = [[ISImageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    ISImageEntity *entity = (ISImageEntity *)[self.images objectAtIndex:indexPath.row];
    
    [cell.thumbnail setImage:entity.image];
    cell.progressBar.progress = entity.progress;
    if (entity.progress == 1) {
        cell.progressBar.hidden = YES;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
     
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (((ISImageEntity*)[self.images objectAtIndex:indexPath.row]).shouldBeDisplayed == NO) {
        return 0;
    }
    return kResultTableCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIActionSheet *actionList = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Save", @"Delete", @"Use this", nil];
    actionList.tag = indexPath.row;
    [actionList showInView:[UIApplication sharedApplication].keyWindow];
}


#pragma mark - Action Sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
//            Save
        case 0: {
            UIImageWriteToSavedPhotosAlbum(((ISImageEntity *)[self.images objectAtIndex:actionSheet.tag]).image, nil, nil, nil);
            break;
        }
//            Remove from result table
        case 1: {
            ((ISImageEntity*)[self.images objectAtIndex:actionSheet.tag]).shouldBeDisplayed = NO;
//            [self.images removeObjectAtIndex:actionSheet.tag];
            if ([self.delegate respondsToSelector:@selector(reloadTableData)]) {
                [self.delegate reloadTableData];
            }
            break;
        }
//            Use image for editing
        case 2: {
            [self.delegate setImageForEditing:((ISImageEntity *)[self.images objectAtIndex:actionSheet.tag]).image];
            break;
        }
        default:
            break;
    }
}

#pragma mark - Image handler delegate

- (void)imageHandlingProgress:(long)index seconds:(float)seconds andTotalSeconds:(float)totalSeconds {

}
- (void)imageHandlingProgressAtIndex:(long)index progress:(float)progress {
    ((ISImageEntity *)[self.images objectAtIndex:index]).progress = progress;
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:0];
    if ([self.delegate respondsToSelector:@selector(cellForRowAtIndex:)]) {
        ISImageCell *cell = (ISImageCell*)[self.delegate cellForRowAtIndex:path];
        
        if (cell) {
            cell.progressBar.progress = progress;
            if (((ISImageEntity *)[self.images objectAtIndex:index]).completed) {
                cell.progressBar.hidden = YES;
            } else {
                cell.progressBar.hidden = NO;
            }
        }
    }
}



- (void)handlingFinishedAtIndex:(long)index withImage:(UIImage *)image {
    ((ISImageEntity *)[self.images objectAtIndex:index]).progress = 1;
    ((ISImageEntity *)[self.images objectAtIndex:index]).image = image;

    
    NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:0];
    if ([self.delegate respondsToSelector:@selector(cellForRowAtIndex:)]) {
        ISImageCell *cell = (ISImageCell*)[self.delegate cellForRowAtIndex:path];
        if (cell) {
            cell.progressBar.progress = 1;
            cell.progressBar.hidden = YES;
            [cell.thumbnail setImage:image];
        }
    }
}

@end

//
//  ISImageHandler.m
//  PhotoEditor
//
//  Created by igor Shvetsov on 28/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import "ISImageHandler.h"
#import "ISImageEditor.h"

@implementation ISImageHandler

- (id)initWithDelegate:(id<ImageHandlerDelegate>)delegate {
    self = [super init];
    if (self) {
        _delegate = delegate;
    }
    
    return self;
}

- (void)handleItWithIndex:(long)index {
    self.total = 30;
    self.ticks = self.total;
    self.countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tick:) userInfo:[NSNumber numberWithLong:index] repeats:YES];
}

- (void)tick:(NSTimer *)timer {
    self.ticks--;
    long index = [(NSNumber *)[timer userInfo] longValue];
    
    if ([self.delegate respondsToSelector:@selector(imageHandlingProgressAtIndex:progress:)]) {
        [self.delegate imageHandlingProgressAtIndex:index progress:(1-self.ticks/self.total)];
    }
    
    if (self.ticks <= 0) {
        [self.countdownTimer invalidate];
        self.countdownTimer = nil;
    }
}

- (void)makeProcessingForImage:(UIImage *)image withIndex:(long)index andFilter:(NSInteger)tag {
    ISImageEditor *imageEditor = [[ISImageEditor alloc] init];
    
    self.total = [self randomBetweenMin:5 andMax:30];
    self.ticks = self.total;
    self.countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tick:) userInfo:[NSNumber numberWithLong:index] repeats:YES];
    
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, self.total * NSEC_PER_SEC);
    dispatch_after(delay, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *newImage = [imageEditor rotateImage:image byDegree:90];
        
        switch (tag) {
            case 0: {
                newImage = [imageEditor rotateImage:image byDegree:90];
                break;
            }
            case 1: {
                newImage = [imageEditor convertImageToGrayScale:image];
                break;
            }
            case 2: {
                newImage = [imageEditor mirrorImageHorizontally:image];
                break;
            }
            default:
                break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([_delegate respondsToSelector:@selector(handlingFinishedAtIndex:withImage:)]) {
                [_countdownTimer invalidate];
                _countdownTimer = nil;
                [_delegate handlingFinishedAtIndex:index withImage:newImage];
            }

        });
    });
    
}

- (NSInteger)randomBetweenMin:(NSInteger)min andMax:(NSInteger)max {
    return min + arc4random_uniform(max - min + 1);
}

- (void)someWork {
    NSLog(@"some work");
}

@end

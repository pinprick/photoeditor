//
//  ISImageCell.h
//  PhotoEditor
//
//  Created by igor Shvetsov on 27/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISImageCell : UITableViewCell

@property (nonatomic) UIImageView *thumbnail;
@property (nonatomic) UIProgressView *progressBar;


@end

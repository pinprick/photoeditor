//
//  ISImageEntity.h
//  PhotoEditor
//
//  Created by igor Shvetsov on 29/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ISImageEntity : NSObject

@property (nonatomic) UIImage *image;
@property (nonatomic) long index;
@property (nonatomic) float progress;

@property (nonatomic) BOOL shouldBeDisplayed;
@property (nonatomic) BOOL completed;

@end

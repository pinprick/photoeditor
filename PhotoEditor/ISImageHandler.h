//
//  ISImageHandler.h
//  PhotoEditor
//
//  Created by igor Shvetsov on 28/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageHandlerDelegate <NSObject>

- (void)imageHandlingProgressAtIndex:(long)index progress:(float)progress;
- (void)handlingFinishedAtIndex:(long)index withImage:(UIImage *)image;

@end

@interface ISImageHandler : NSObject

@property (nonatomic, weak) id<ImageHandlerDelegate> delegate;
@property (nonatomic) float total;
@property (nonatomic) float ticks;
@property (nonatomic) NSTimer *countdownTimer;

- (id)initWithDelegate:(id<ImageHandlerDelegate>)delegate;
- (void)handleItWithIndex:(long)index;

- (void)makeProcessingForImage:(UIImage *)image withIndex:(long)index andFilter:(NSInteger)tag;

@end

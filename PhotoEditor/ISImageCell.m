//
//  ISImageCell.m
//  PhotoEditor
//
//  Created by igor Shvetsov on 27/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import "ISImageCell.h"
#import "Constants.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width

@implementation ISImageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:_thumbnail];
    }
    
    return self;
}

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _thumbnail = [[UIImageView alloc] init];
        [_thumbnail setContentMode: UIViewContentModeScaleAspectFit];
        [self.contentView addSubview:_thumbnail];
        _progressBar = [[UIProgressView alloc] init];
        _progressBar.progress = 0;
        [self.contentView addSubview:_progressBar];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect imageFrame = CGRectMake(self.contentView.frame.size.width/4.0, 0, self.contentView.frame.size.width/2.0, self.contentView.frame.size.height);
    CGRect progressFrame = CGRectMake(20, kResultTableCellHeight/2.0, self.contentView.frame.size.width - 40, 10);
    self.thumbnail.frame = imageFrame;
    self.progressBar.frame = progressFrame;
    
}





@end

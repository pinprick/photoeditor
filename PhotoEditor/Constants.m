//
//  Constants.m
//  PhotoEditor
//
//  Created by igor Shvetsov on 27/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import <UIKit/UIKit.h>

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width

@implementation Constants

// Filter buttons constants
float const kFilterButtonWidth = 100.0;
float const kFilterButtonHeight = 30.0;
float const kFilterButtonXOffset;
float const kFilterButtonYOffset = 50.0;
float const kFilterButtonRightOffset = 20.0;

float const kFilterButtonFontSize = 15.0;

// Image picker constants
float const kImagePickerWidth = 120.0;
float const kImagePickerHeight = 120.0;
float const kImagePickerXOffset = 20.0;
float const kImagePickerYOffset = 50.0;
float const kImagePickerBorderWidth = 1.0;

float const kImagePickerFontSize = 15.0;

// Result table constants
float const kResultTableCellHeight = 80.0;




@end





//
//  ISResultsViewController.h
//  PhotoEditor
//
//  Created by igor Shvetsov on 26/07/15.
//  Copyright (c) 2015 igor Shvetsov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISImageHandler.h"

@interface ISResultsViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, ImageHandlerDelegate>

@property (nonatomic) NSMutableArray *images;

@property (nonatomic, weak) id delegate;
@property (nonatomic) ISImageHandler *imageHandler;

@end
